# TerribleSound

"Automated" "all-in-one" "easy to use" keyboard-triggered soundboard. 
 
Written for Terriblehack XIII

USAGE:

Place terriblesound.py in a (preferably empty folder)
have a subfolder called "sounds" which itself has subfolders a-z, which should
be populated with the sounds you'd like to play, and have a blank mp3 file...

or just download and extract sounds.zip!

then run the .py file and have fun!


Windows only. Could probably be adapted to Linux and OSX by changing the 
backslashes to frontslashes, and using an alternative to pynput to capture 
keypresses

