from pynput.keyboard import Key, Listener
import os, threading, random, time, soundfile
from playsound import playsound
from mutagen.mp3 import MP3

rootdir = os.getcwd() + "\\sounds\\"
dirlist = os.listdir(rootdir)


def rndm_file(ttt):
    global rootdir
    files = os.listdir(ttt)
    if len(files) == 0:
        return rootdir + "blank.mp3"
    else:
        result = ttt+"\\"+ random.sample(files,1)[0]
        return result



def fnamegen(keypress):
    rslt = rootdir+ "blank.mp3"
    for i in dirlist:
        if i == keypress:
            rslt = rndm_file(rootdir + i)
            break
    return rslt


length = 0
oldtime = time.time()

def play(name):
    global length
    global oldtime
    if oldtime+length<time.time():
        t=threading.Thread(target=playsound, args = [name])
        if name[-3:] == "wav":
            f = soundfile.SoundFile(name)
            length = len(f)/f.samplerate
        elif name[-3:] == "mp3":
            b = MP3(name)
            length = b.info.length

        oldtime = time.time()
        t.start()



def on_press(key):
    try:
        print(key.char)
        currname = fnamegen(chr(96+(ord(key.char) % 96)))
        play(currname)
    except:
        pass

with Listener(on_press=on_press) as listener:
	listener.join()




	
